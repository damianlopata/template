################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC1352R1_LAUNCHXL_NoRTOS.cmd 

C_SRCS += \
../CC1352R1_LAUNCHXL.c \
../CC1352R1_LAUNCHXL_fxns.c \
../ccfg.c \
../display.c \
../main_nortos.c \
../splash_image.c 

C_DEPS += \
./CC1352R1_LAUNCHXL.d \
./CC1352R1_LAUNCHXL_fxns.d \
./ccfg.d \
./display.d \
./main_nortos.d \
./splash_image.d 

OBJS += \
./CC1352R1_LAUNCHXL.obj \
./CC1352R1_LAUNCHXL_fxns.obj \
./ccfg.obj \
./display.obj \
./main_nortos.obj \
./splash_image.obj 

OBJS__QUOTED += \
"CC1352R1_LAUNCHXL.obj" \
"CC1352R1_LAUNCHXL_fxns.obj" \
"ccfg.obj" \
"display.obj" \
"main_nortos.obj" \
"splash_image.obj" 

C_DEPS__QUOTED += \
"CC1352R1_LAUNCHXL.d" \
"CC1352R1_LAUNCHXL_fxns.d" \
"ccfg.d" \
"display.d" \
"main_nortos.d" \
"splash_image.d" 

C_SRCS__QUOTED += \
"../CC1352R1_LAUNCHXL.c" \
"../CC1352R1_LAUNCHXL_fxns.c" \
"../ccfg.c" \
"../display.c" \
"../main_nortos.c" \
"../splash_image.c" 


